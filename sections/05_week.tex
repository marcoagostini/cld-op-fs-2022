\section{Another Introduction Kubernetes}

\subsection{Management}
The recommended way to work with kubectl, is by writing your manifest files and using \lstinline|kubectl create/apply -f manifest.yaml|. This declarative methodology is giving you much more control than the imperative methodology where you create all form the CLI.

\begin{lstlisting}
kubectl create -f nginx.yaml
kubectl replace -f nginx.yaml
kubectl apply -f nginx.yaml
\end{lstlisting}

\subsubsection{Update Strategies}
It is the task of the deployment to ensure that a sufficient number of Pods is running at all times.  So when making a change, this change is applied as a rolling update: the changed version is deployed and after that has confirmed to be successful, the old version is taken offline.  You can use \lstinline|kubectl rollout history| to get details about recent transactions. Use \lstinline|kubectl rollout undo| to undo a previous change.

\paragraph{Recreate} \hfill \\
All Pods are killed and new Pods are created. This will lead to temporary unavailability. Useful if you cannot simultaneously run different versions of an application 

\paragraph{RollingUpdate} \hfill \\
Updates Pods one at a time to guarantee availability of the application. This is the preferred approach, and you can further tune its behavior


\subsection{Architecture}
The base objects of Kubernetes are: Pod, Service, Volume, Namespace.

\subsubsection{Pods}
A Pod is an abstraction of a server. It runs on one or multiple containers within a single name space, exposed by a single address. The Pod is the minimal entity that is managed by Kubernetes. Typically Pods are only started through a Deployment, because naked Pods are not rescheduled in case of a node failure. A Pod (as in a pod of whales or pea pod) is a group of one or more containers, with shared storage and network resources, and a specification for how to run the containers. A Pod's contents are always co-located and co-scheduled, and run in a shared context.

\paragraph{Configuration} \hfill
\begin{lstlisting}
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.14.2
    ports:
    - containerPort: 80
\end{lstlisting}

\subsubsection{Namespaces}
In Kubernetes, namespaces provides a mechanism for isolating groups of resources within a single cluster. Names of resources need to be unique within a namespace, but not across namespaces. Namespace-based scoping is applicable only for namespaced objects (e.g. Deployments, Services, etc) and not for cluster-wide objects (e.g. StorageClass, Nodes, PersistentVolumes, etc).

\begin{lstlisting}
kubectl get namespace

kubectl run nginx --image=nginx --namespace=<insert-namespace-name-here>
kubectl get pods --namespace=<insert-namespace-name-here>
\end{lstlisting}


\subsubsection{Services}
An abstract way to expose an application running on a set of Pods as a network service. With Kubernetes you don't need to modify your application to use an unfamiliar service discovery mechanism. Kubernetes gives Pods their own IP addresses and a single DNS name for a set of Pods, and can load-balance across them.

\paragraph{Configuration} \hfill

\begin{lstlisting}
apiVersion: v1
kind: Service
metadata:
  name: my-service
spec:
  selector:
    app: MyApp
  ports:
    - protocol: TCP
      port: 80
      targetPort: 9376
\end{lstlisting}

\subsubsection{Ingress}
Ingress exposes HTTP and HTTPS routes from outside the cluster to services within the cluster. Traffic routing is controlled by rules defined on the Ingress resource. An Ingress may be configured to give Services externally-reachable URLs, load balance traffic, terminate SSL / TLS, and offer name-based virtual hosting. An Ingress controller is responsible for fulfilling the Ingress, usually with a load balancer, though it may also configure your edge router or additional frontends to help handle the traffic.
 
\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{ingress}
  \caption{Ingress}
\end{figure}

\subsubsection{Ressource Limitations}
By default, a Pod will use as much CPU and memory as necessary to do its work. This can be managed by using Memory/CPU Requests and Limits in pod.spec.containers.resources. Memory limits can be set also, and are converted to the \lstinline|--memory| option that can be used by the docker run command (or anything similar) 

\begin{lstlisting}
---
apiVersion: v1
kind: Pod
metadata:
  name: frontend
spec:
  containers:
  - name: app
    image: images.my-company.example/app:v4
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
\end{lstlisting}

\subsection{Storage}
On-disk files in a container are ephemeral, which presents some problems for non-trivial applications when running in containers. One problem is the loss of files when a container crashes. The kubelet restarts the container but with a clean state.

\begin{figure}[h!]
  \center
  \includegraphics[width=0.75\textwidth]{kubernetesstorage}
  \caption{Kubernets Storage}
\end{figure}

\subsubsection{Storage Class}
 Cluster environments typically have their own specific storage type. Kubernetes StorageClass allows for automatic provisioning of PVs when a PVC request for storage comes in. StorageClass must be backed by a storage provisioner, which takes care of the volume configuration. Kubernetes comes with some internal provisioners, alternatively external provisioners can be installed using Operators.

\subsubsection{Persistant Volumes}
Kubernetes supports many types of volumes. A Pod can use any number of volume types simultaneously. Ephemeral volume types have a lifetime of a pod, but persistent volumes exist beyond the lifetime of a pod. When a pod ceases to exist, Kubernetes destroys ephemeral volumes; however, Kubernetes does not destroy persistent volumes. For any kind of volume in a given pod, data is preserved across container restarts.

A PV is an independent resource that connects to external storage. The PVC talks to the available backend storage provider and dynamically uses volumes that are available on that storage type.

\begin{lstlisting}
apiVersion: v1
kind: PersistentVolume
metadata:
  name: foo-pv
spec:
  storageClassName: ""
  claimRef:
    name: foo-pvc
    namespace: foo
\end{lstlisting}

\subsubsection{Persistent Volume Claim}
To bind a pod to a PV, the pod must contain a volume mount and a PVC. These declarations allow users to mount PVs in pods without knowing the details of the underlying storage equipment. A PVC bind is exclusive: after successfully binding to a PV, the PV cannot be used by other PVC anymore. If a perfect match is not available, StorageClass may kick in and dynamically create a PV that does exactly match the request.

\begin{lstlisting}
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: foo-pvc
  namespace: foo
spec:
  storageClassName: "" # Empty string must be explicitly set otherwise default StorageClass will be set
  volumeName: foo-pv
  ...
\end{lstlisting}

\subsection{ConfigMaps}
 It's a good idea to separate site-specific information from code. Code should be static, which makes it portable, so that it can be used in other environments.  Kubernetes provides ConfigMaps to deal with this issue. A ConfigMap can be used to define variables and the Deployment just points to the ConfigMap.
 
 \begin{lstlisting}
 apiVersion: v1
kind: ConfigMap
metadata:
  name: game-demo
data:
  # property-like keys; each key maps to a simple value
  player_initial_lives: "3"
  ui_properties_file_name: "user-interface.propertie
 \end{lstlisting}


\subsection{Secrets}
Secrets allow for storage of sensitive data such as passwords, Auth tokens and SSH keys. Using Secrets reduces the risk of accidental exposure of confidential information. Some Secrets are automatically created by the system, users can also use secrets. System-created Secrets are important for Kubernetes resources to connect to other cluster resources. Secrets are not encrypted, they are base64 encoded.

There are three different secret-types: docker-registry, TLS, generic (any value).

\subsection{Probes}
Probes can be used to test access to pods and they are part of the pod specification. A readinessProbe is used to make sure a Pod is not published as available until the readinessProbe has been able to access it. The livenessProbe is used to continue checking the availability of a pod.









